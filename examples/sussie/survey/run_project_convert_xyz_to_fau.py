import logging
import os

from oshydro.sussie.common.logging import LoggingSetup
from oshydro.sussie.common.testing import Testing
from oshydro.sussie.survey.project import Project

LoggingSetup.set_logging(ns_list=["oshydro.sussie"])
logger = logging.getLogger(__name__)

# This example takes .xyz files and creates headerless FAU files.

# Set a path to a xyz file and prepare the output FAU path

# test_geographic_depthless.xyz can be used to test nodata value

testing = Testing()
input_paths = testing.input_test_files(ext='.xyz', subfolder='xyz')
logger.debug("nr. of files: %s" % len(input_paths))

# Actually creating the FAU files
for input_path in input_paths:
    logger.debug('input: %s' % input_path)
    output_path = os.path.join(testing.output_data_folder(),
                               '%s.fau' % os.path.splitext(os.path.basename(input_path))[0])
    logger.debug('output: %s' % output_path)
    prj = Project()
    if "test_geographic.xyz" in input_path:
        ret = prj.convert_from_xyz(input_path=input_path, output_path=output_path, input_epsg_code=4258,
                                   output_epsg_code=25832, skip_rows=1, z_is_elevation=True)
    if "test_geographic_time.xyz" in input_path:
        ret = prj.convert_from_xyz(input_path=input_path, output_path=output_path, time_idx=3, input_epsg_code=4258,
                                   output_epsg_code=25832, skip_rows=1, z_is_elevation=True)
    else:
        ret = prj.convert_from_xyz(input_path=input_path, output_path=output_path, skip_rows=1, z_is_elevation=True)
    logger.debug('generated: %s' % ret)
