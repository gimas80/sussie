import logging
import os

from oshydro.sussie.common.logging import LoggingSetup
from oshydro.sussie.common.testing import Testing
from oshydro.sussie.survey.project import Project

LoggingSetup.set_logging(ns_list=["oshydro.sussie"])
logger = logging.getLogger(__name__)

# This example takes a folder path and converts all xyz files to header-less FAU file.

testing = Testing()

# input/output settings:
input_folder = os.path.join(testing.input_data_folder(), "xyz")  # Folder containing xyz files
output_folder = os.path.join(testing.output_data_folder(), "xyz")  # if None files will be placed in input folder
recursive_scan = True  # Dive into subfolders for finding xyz files
overwrite_fau = False

# xyz parsing settings:
skip_rows = 1  # Skip potential header rows
column_separator = ","
column_index_x = 0
column_index_y = 1
column_index_z = 2

# Transformation settings:
z_is_elevation = True  # False if z is depth
deep_shift = None  # Apply shift as decimal number
little_endian = True

# Converting xyz to fau
prj = Project()
logger.info("Processing folder {}".format(input_folder))
files_converted = 0
files_skipped = 0
for base, folders, files in os.walk(os.path.abspath(input_folder)):
    for file in files:
        if os.path.splitext(file)[1].lower() != ".xyz":
            continue

        input_path = os.path.join(base, file)
        output_path = os.path.join('%s.fau' % os.path.splitext(input_path)[0])

        if output_folder is not None:
            output_path = output_path.replace(os.path.abspath(input_folder),
                                              os.path.abspath(output_folder))
            os.makedirs(os.path.dirname(output_path), exist_ok=True)

        if not overwrite_fau and os.path.exists(output_path):
            logger.warning("Fau file already exists. Skipping: {}".format(output_path))
            files_skipped += 1
            continue

        logger.info("Converting file nr. {}: {} -> {}".format(files_converted + 1, input_path, output_path))
        ret = prj.convert_from_xyz(input_path=input_path,
                                   output_path=output_path,
                                   skip_rows=skip_rows,
                                   little_endian=True,
                                   depth_shift=deep_shift,
                                   easting_idx=column_index_x,
                                   northing_idx=column_index_y,
                                   depth_idx=column_index_z,
                                   split_value=column_separator,
                                   z_is_elevation=z_is_elevation)
        files_converted += 1
    if not recursive_scan:
        break

logger.info("nr. of files converted/skipped: {}/{}".format(files_converted, files_skipped))
