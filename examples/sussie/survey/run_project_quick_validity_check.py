import logging

from oshydro.sussie.common.logging import LoggingSetup
from oshydro.sussie.common.testing import Testing
from oshydro.sussie.survey.project import Project

LoggingSetup.set_logging(ns_list=["oshydro.sussie"])
logger = logging.getLogger(__name__)

# This example runs a quick validity check on a list of files

testing = Testing()
input_paths = testing.input_test_files(ext='.fau', subfolder='fau')
logger.debug("nr. of files: %s" % len(input_paths))

prj = Project()

nr_faus = len(input_paths)
for idx, input_fau in enumerate(input_paths):
    prj.quick_validity_check(input_fau)
    logger.info('valid -> %d/%d (%s)' % (idx, nr_faus - 1, input_fau))
