import logging
import os

from oshydro.sussie.common.logging import LoggingSetup
from oshydro.sussie.common.testing import Testing
from oshydro.sussie.survey.project import Project

LoggingSetup.set_logging(ns_list=["oshydro.sussie"])
logger = logging.getLogger(__name__)

# This example takes a .shp file and create a header-less FAU file.

# Set a path to a shp file and prepare the output FAU path

testing = Testing()
input_path = r"PATH\TO\SHAPE.shp"
logger.debug('input: %s' % input_path)
output_path = os.path.join(testing.output_data_folder(), '%s.fau' % os.path.splitext(os.path.basename(input_path))[0])
logger.debug('output: %s' % output_path)

# Actually creating the FAU file

prj = Project()
ret = prj.convert_from_shp(input_path=input_path, output_path=output_path)
logger.debug('generated: %s' % ret)
