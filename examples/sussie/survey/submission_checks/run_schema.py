import logging

from oshydro.sussie.common.logging import LoggingSetup
from oshydro.sussie.survey.project import Project
from oshydro.sussie.survey.submission_checks.schema import Schema

LoggingSetup.set_logging(ns_list=["oshydro.sussie"])
logger = logging.getLogger(__name__)

# This example lists and visualizes the content of the schema files for the Submission Check tool

schema_dict = Project.schema_dict()
logger.debug("Schemas: \n%s" % schema_dict)

schema = Schema(json_path=Project.schema_list()[0])
logger.debug(schema)

logger.debug(type(schema.json))
