from oshydro.sussie.common.logging import LoggingSetup
from oshydro.sussie.app.gui import gui

LoggingSetup.set_logging(ns_list=["oshydro.sussie"])

gui()
