import os
import unittest

from oshydro.sussie.common.helper import Helper


class TestCommonHelper(unittest.TestCase):

    def test_explore_folder(self):
        self.assertTrue(Helper.explore_folder(__file__))
        self.assertFalse(Helper.explore_folder(__file__ + ".fake"))
        self.assertTrue(Helper.explore_folder(os.path.dirname(__file__)))
        self.assertFalse(Helper.explore_folder(os.path.dirname(__file__) + "fake"))

    def test_first_match(self):
        # fake dict
        a_dict = {
            "a": 1,
            "b": 99,
            "c": 1,
        }

        # test if it gives back the first matching key
        self.assertTrue(Helper.first_match(a_dict, 1) in ["a", "c"])

        # test if it raises with a not-existing value
        with self.assertRaises(RuntimeError):
            Helper.first_match(a_dict, 2)

    def test_is_64bit_os(self):
        self.assertIsInstance(Helper.is_64bit_os(), bool)

    def test_is_64bit_python(self):
        self.assertIsInstance(Helper.is_64bit_python(), bool)

    def test_is_darwin_linux_windows(self):
        self.assertIsInstance(Helper.is_darwin(), bool)
        self.assertIsInstance(Helper.is_linux(), bool)
        self.assertIsInstance(Helper.is_windows(), bool)

        self.assertTrue(any([Helper.is_linux(), Helper.is_darwin(), Helper.is_windows()]))

    def test_is_pydro(self):
        self.assertIsInstance(Helper.is_pydro(), bool)

    def test_is_url(self):
        self.assertTrue(Helper.is_url("https://www.oshydro.org"))
        self.assertTrue(Helper.is_url("http://www.oshydro.org"))
        self.assertFalse(Helper.is_url("ftp://fake/url"))

    def test_python_path(self):
        self.assertTrue(os.path.exists(Helper.python_path()))

    def test_package_info(self):
        self.assertIsInstance(Helper.package_info(qt_html=True), str)
        self.assertIsInstance(Helper.package_info(qt_html=False), str)

    def test_package_folder(self):
        self.assertTrue(os.path.exists(Helper.package_folder()))

    def test_oshydro_folder(self):
        self.assertTrue(os.path.exists(Helper.oshydro_folder()))


def suite():
    s = unittest.TestSuite()
    s.addTests(unittest.TestLoader().loadTestsFromTestCase(TestCommonHelper))
    return s
