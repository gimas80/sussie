import os
import unittest

from oshydro.sussie.common.testing import Testing
from oshydro.sussie.common.helper import Helper


class TestCommonTesting(unittest.TestCase):

    def setUp(self):

        self.t = Testing()

    def test_root_folder(self):
        self.assertTrue(os.path.exists(self.t.root_folder))
        self.assertGreater(len(Helper.files(folder=self.t.root_folder, ext=".fau")), 0)

    def test_input_data(self):
        self.assertTrue(os.path.exists(self.t.input_data_folder()))
        self.assertGreater(len(self.t.input_test_files(ext="")), 0)
        self.assertGreaterEqual(len(self.t.input_data_sub_folders()), 0)

    def test_download_data(self):
        self.assertTrue(os.path.exists(self.t.download_data_folder()))
        self.assertGreaterEqual(len(self.t.download_test_files(ext="")), 0)

    def test_temp_data(self):
        self.assertTrue(os.path.exists(self.t.temp_data_folder()))
        self.assertGreaterEqual(len(self.t.temp_test_files(ext="")), 0)

    def test_output_data(self):
        self.assertTrue(os.path.exists(self.t.output_data_folder()))
        self.assertGreaterEqual(len(self.t.output_test_files(ext="")), 0)


def suite():
    s = unittest.TestSuite()
    s.addTests(unittest.TestLoader().loadTestsFromTestCase(TestCommonTesting))
    return s
